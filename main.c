#include <stdio.h>
#include <stdlib.h>

#include "phone_number.h"

void help(char *prog[])
{
    printf("hasznalata %s: %s <sring>telefonszam\n");
}

int main(int argc, char* argv[])
{
    if(argc == 1)
    {
        help(argv[0]);
        return 0;
    }

    int ret = test_phone_number(argv[1]);

    if(!ret)
    {
        printf("A szam megfelelo!\n");
        return 0;
    }
    else
    {
        printf("Hiba lepett fel a program futasa kozben\n");
        switch (ret)
        {
        case 1:
            printf("Hibas mobil szam. Plussz es per nem lehet egyszerre.\n");
            break;
        case 2:
            printf("A mobil szam hibas: csak szam lehet benne!\n");
            break;
        case 3:
            printf("Tul rovid a szam!\n");
            break;
        case 4:
            printf("Tul hosszu a szam!\n");
            break;
        case 5:
            printf("A '\\'-es szam formatum nem megfelelo!\n");
            break;
        case 6:
            printf("A '+'-os szam nem megfelelo!\n");
            break;
        case 7:
            printf("Hianyzik a '+' vagy a '\\'!\n");
            break;
        default:
            printf("A beadott ertek NULL volt!\n");
        }
        return 0;
    }
}
