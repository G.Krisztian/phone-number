#include <stdio.h>


#include "Phone_number.h"


int test_phone_number(char* number)
{


    if(!number)
    {
        return NULL;
    }

    int plus = 0;
    int slash = 0;
    int index;

    for(index=0; number[index] != NULL && number[index] != 0; index++)
    {
        if(index == NULL)
        {
            if(number[index] == '+')
            {
                plus = 1;
                continue;
            }
        }

        if(index==2)
        {
            if(number[index] == '/')
            {
                if(plus)
                {
                    return 1;
                }
                slash = 1;
                continue;
            }
        }
        if(number[index] < '0'&& number > '9')
        {
            return 2;
        }
    }
    if(index < 9)
    {
        return 3;
    }

    if(index > 12)
    {
        return 4;
    }

    if(slash && index != 9)
    {
        return 5;
    }
    if(plus && index != 12)
       {
            return 6;
       }
    if(!(plus || slash))
    {
        return 7;
    }

    return 0;


}
